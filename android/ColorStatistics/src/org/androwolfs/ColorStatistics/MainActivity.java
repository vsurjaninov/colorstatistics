package org.androwolfs.ColorStatistics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

public class MainActivity extends Activity {

    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void toMapRegions(View view){
        Intent intent = new Intent(view.getContext(), RegionsMap.class);
        startActivity(intent);
    }

    public void toStatistics_list(View view){
        Intent intent = new Intent(view.getContext(), CategoriesList.class);
        startActivity(intent);
    }

}
package org.androwolfs.ColorStatistics;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import org.androwolfs.ColorStatistics.models.CategoryProperties;
import org.androwolfs.ColorStatistics.models.RegionStatistic;
import org.androwolfs.ColorStatistics.polygon.JSONArrayPolygon;
import org.androwolfs.ColorStatistics.polygon.PolygonInfoWindow;
import org.androwolfs.ColorStatistics.utils.HttpLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class StatisticMap extends Activity{
    private MapView map;
    private ArrayList<RegionStatistic> regionStatistics;
    private Context context = this;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regions_map);

        regionStatistics = getIntent().getParcelableArrayListExtra("regions");
        final String category = getIntent().getStringExtra("category");
        final TextView headerTextView = (TextView) findViewById(R.id.header);
        headerTextView.setText(category + " " + CategoryProperties.getUnits(context, category));

        map = (MapView) findViewById(R.id.mapview);
        map.setTileSource(TileSourceFactory.MAPQUESTOSM);
        map.setMultiTouchControls(true);

        IMapController mapController = map.getController();
        mapController.setZoom(11);
        mapController.setCenter(new GeoPoint(59.8944444, 30.2641667));

        Toast.makeText(getBaseContext(), context.getText(R.string.loading), Toast.LENGTH_LONG).show();
        if(HttpLoader.isConnected(this)){
            new HttpAsyncTask().execute("http://androwolfs.cloudapp.net/polygons");
        } else {
            Toast.makeText(getBaseContext(), context.getText(R.string.connection_error), Toast.LENGTH_LONG).show();
        }
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return HttpLoader.GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            int[] yellow = {255, 170, 5};
            int[] green = {71, 194, 71};
            int[] blue = {0, 180, 245};
            int[] red = {255, 51, 51};

            ArrayList<int[]> colors = new ArrayList<int[]>();
            colors.add(yellow);
            colors.add(green);
            colors.add(blue);
            colors.add(red);

            int[] color = colors.get((new Random()).nextInt(4));

            float max = Float.parseFloat(Collections.max(regionStatistics).getValue());
            float min = Float.parseFloat(Collections.min(regionStatistics).getValue());

            try{
                final JSONParser parser = new JSONParser();
                final JSONObject jsonRegions = (JSONObject)parser.parse(result);

                for(RegionStatistic regionStatistic: regionStatistics){
                    String region = regionStatistic.getRegionName();
                    final JSONArrayPolygon polygon = new JSONArrayPolygon(map.getContext());
                    polygon.setJSONPoints((JSONArray)jsonRegions.get(region));

                    PolygonInfoWindow infoWindow = new PolygonInfoWindow(map, region, regionStatistic.getValue());
                    polygon.setInfoWindow(infoWindow);

                    float value = Float.parseFloat(regionStatistic.getValue());
                    int opancy = (int) (64 + 127*(value - min)/(max - min));
                    final int intColor = Color.argb(opancy, color[0], color[1], color[2]);

                    polygon.setFillColor(intColor);
                    polygon.setStrokeColor(Color.BLACK);
                    polygon.setStrokeWidth(2);
                    polygon.setStrokeColor(getResources().getColor(R.color.dark));
                    polygon.setStrokeWidth(2);

                    map.getOverlays().add(polygon);
                }
                map.invalidate();
                Toast.makeText(getBaseContext(), context.getText(R.string.ready), Toast.LENGTH_LONG).show();

            } catch (Exception e){
                Toast.makeText(getBaseContext(), context.getText(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        }
    }
}

package org.androwolfs.ColorStatistics.models;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import org.json.simple.JSONObject;

public class RegionInfo implements Parcelable{

    private final String name;
    private final Bundle info;

    public RegionInfo(String name, Bundle info){
        this.name = name;
        this.info = info;
    }

    public RegionInfo(Context context, String name, JSONObject jsonObject){
        super();
        this.name = name;
        info = new Bundle();
        for(Category key: Category.values()){
            String categoryName = CategoryProperties.getName(context, key);
            info.putString(categoryName, (String)jsonObject.get(categoryName));
        }
    }

    public String getName() {
        return name;
    }

    public Bundle getInfo() {
        return info;
    }

    public String getValue(String category){
        return info.getString(category);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeBundle(info);
    }

    public static final Parcelable.Creator<RegionInfo> CREATOR
            = new Parcelable.Creator<RegionInfo>() {
        public RegionInfo createFromParcel(Parcel in) {
            return new RegionInfo(in);
        }

        public RegionInfo[] newArray(int size) {
            return new RegionInfo[size];
        }
    };

    private RegionInfo(Parcel in) {
       name = in.readString();
       info = in.readBundle();
    }
}

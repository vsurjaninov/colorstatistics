package org.androwolfs.ColorStatistics.models;

import android.os.Parcel;
import android.os.Parcelable;

public class RegionStatistic implements Parcelable, Comparable<RegionStatistic> {
    final private String regionName;
    final private String value;

    public RegionStatistic(String regionName, String value){
        this.regionName = regionName;
        this.value = value;
    }

    public String getRegionName() {
        return regionName;
    }

    public String getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(regionName);
        dest.writeString(value);
    }

    public static final Parcelable.Creator<RegionStatistic> CREATOR
            = new Parcelable.Creator<RegionStatistic>() {
        public RegionStatistic createFromParcel(Parcel in) {
            return new RegionStatistic(in);
        }

        public RegionStatistic[] newArray(int size) {
            return new RegionStatistic[size];
        }
    };

    private RegionStatistic(Parcel in){
        regionName = in.readString();
        value = in.readString();
    }

    @Override
    public int compareTo(RegionStatistic another) {
        return Float.compare(Float.parseFloat(value), Float.parseFloat(another.getValue()));
    }
}

package org.androwolfs.ColorStatistics.models;

import android.content.Context;
import org.androwolfs.ColorStatistics.R;

public class CategoryProperties {
    public static String getName(Context context, Category category){
        switch (category) {
            case BIRTHRATE:
                return context.getResources().getString(R.string.birthrate);
            case POPULATION:
                return context.getResources().getString(R.string.population);
            case KINDERGARDENS:
                return context.getResources().getString(R.string.kindergardens);
            case AVAILABLE_APARTAMENTS:
                return context.getResources().getString(R.string.available_apartaments);
            case CRIME:
                return context.getResources().getString(R.string.crime);
            case STOLEN_CARS:
                return context.getResources().getString(R.string.stolen_cars);
            case SPORT_FACILITIES:
                return context.getResources().getString(R.string.sport_facilities);
            case LIBRARIES:
                return context.getResources().getString(R.string.libraries);
            case CINEMA:
                return context.getResources().getString(R.string.cinema);
            case HOTELS:
                return context.getResources().getString(R.string.hotels);
            case RADIATION:
                return context.getResources().getString(R.string.radiation);
            case WORKS:
                return context.getResources().getString(R.string.works);
            case ROAD_REPAIRS:
                return context.getResources().getString(R.string.road_repairs);
            case CULTURAL:
                return context.getResources().getString(R.string.cultural);
            default:
                return "null";
        }
    }

    public static String getUnits(Context context, Category category){
        switch (category) {
            case BIRTHRATE:
                return context.getResources().getString(R.string.birthrate_units);
            case POPULATION:
                return context.getResources().getString(R.string.population_units);
            case KINDERGARDENS:
                return context.getResources().getString(R.string.kindergardens_units);
            case AVAILABLE_APARTAMENTS:
                return context.getResources().getString(R.string.available_apartaments_units);
            case CRIME:
                return context.getResources().getString(R.string.crime_units);
            case STOLEN_CARS:
                return context.getResources().getString(R.string.stolen_cars_units);
            case SPORT_FACILITIES:
                return context.getResources().getString(R.string.sport_facilities_units);
            case LIBRARIES:
                return context.getResources().getString(R.string.libraries_units);
            case CINEMA:
                return context.getResources().getString(R.string.cinema_units);
            case HOTELS:
                return context.getResources().getString(R.string.hotels_units);
            case RADIATION:
                return context.getResources().getString(R.string.radiation_units);
            case WORKS:
                return context.getResources().getString(R.string.works_units);
            case ROAD_REPAIRS:
                return context.getResources().getString(R.string.road_repairs_units);
            case CULTURAL:
                return context.getResources().getString(R.string.cultural_units);
            default:
                return "null";
        }
    }

    public static String getUnits(Context context, String categoryName){
        if(categoryName.equals(context.getResources().getString(R.string.birthrate))){
            return context.getResources().getString(R.string.birthrate_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.population))){
            return context.getResources().getString(R.string.population_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.kindergardens))){
            return context.getResources().getString(R.string.kindergardens_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.available_apartaments))){
            return context.getResources().getString(R.string.available_apartaments_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.crime))){
            return context.getResources().getString(R.string.crime_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.stolen_cars))){
            return context.getResources().getString(R.string.stolen_cars_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.sport_facilities))){
            return context.getResources().getString(R.string.sport_facilities_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.libraries))){
            return context.getResources().getString(R.string.libraries_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.cinema))){
            return context.getResources().getString(R.string.cinema_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.hotels))){
            return context.getResources().getString(R.string.hotels_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.radiation))){
            return context.getResources().getString(R.string.radiation_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.works))){
            return context.getResources().getString(R.string.works_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.road_repairs))){
            return context.getResources().getString(R.string.road_repairs_units);
        }
        if(categoryName.equals(context.getResources().getString(R.string.cultural))){
            return context.getResources().getString(R.string.cultural_units);
        }
        return "";
    }
}

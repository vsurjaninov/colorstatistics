package org.androwolfs.ColorStatistics.models;

import android.content.Context;
import android.os.Bundle;
import java.util.ArrayList;

public class InfoModel{
    final private String category;
    final private String value;

    public InfoModel(String category, String value){
        this.category = category;
        this.value = value;
    }

    public static ArrayList<InfoModel> createInfoModels(Context context, RegionInfo regionInfo){
        ArrayList<InfoModel> infoModels = new ArrayList<InfoModel>();
        Bundle infoBundle = regionInfo.getInfo();
        for(String category: infoBundle.keySet()){
            infoModels.add(new InfoModel(category + " " + CategoryProperties.getUnits(context, category),
                    infoBundle.getString(category)));
        }
        return infoModels;
    }

    public String getCategory() {
        return category;
    }

    public String getValue() {
        return value;
    }
}

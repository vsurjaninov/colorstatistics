package org.androwolfs.ColorStatistics.models;

public enum Category {
    BIRTHRATE,
    POPULATION,
    CRIME,
    KINDERGARDENS,
    AVAILABLE_APARTAMENTS,
    STOLEN_CARS,
    SPORT_FACILITIES,
    LIBRARIES,
    CINEMA,
    HOTELS,
    RADIATION,
    WORKS,
    ROAD_REPAIRS,
    CULTURAL
}
package org.androwolfs.ColorStatistics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import org.androwolfs.ColorStatistics.models.Category;
import org.androwolfs.ColorStatistics.models.CategoryProperties;


public class CategoriesList extends Activity implements View.OnClickListener {

    private ListView listView;
    private String[] categoriesNames;
    private Category[] categories = Category.values();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_list);

        listView = (ListView) findViewById(R.id.categoriesListView);
        categoriesNames = new String[categories.length];
        for(int i = 0; i < categories.length; i++){
            categoriesNames[i] = CategoryProperties.getName(this, categories[i]);
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.category, categoriesNames);
        listView.setAdapter(adapter);
    }


    @Override
    public void onClick(View view){
        int position = listView.getPositionForView(view);
        Intent intent = new Intent(this, StatisticRegions.class);
        intent.putExtra("selected_category", categoriesNames[position]);
        startActivity(intent);
    }
}

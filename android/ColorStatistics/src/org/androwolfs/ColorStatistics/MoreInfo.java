package org.androwolfs.ColorStatistics;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.androwolfs.ColorStatistics.adapters.RegionInfoAdapter;
import org.androwolfs.ColorStatistics.models.CategoryProperties;
import org.androwolfs.ColorStatistics.models.RegionInfo;
import org.androwolfs.ColorStatistics.utils.HttpLoader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class MoreInfo extends Activity {
    String regionName;
    Context context = this;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moreinfo);

        regionName = getIntent().getStringExtra("region_name");
        final TextView textView = (TextView) findViewById(R.id.header);
        textView.setText(regionName + " район");

        if(HttpLoader.isConnected(this)){
            new HttpAsyncTask().execute("http://androwolfs.cloudapp.net/statistic/" + regionName);
        } else {
            Toast.makeText(getBaseContext(), context.getText(R.string.connection_error), Toast.LENGTH_LONG).show();
        }
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return HttpLoader.GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            try{
                final JSONParser parser = new JSONParser();
                final RegionInfo regionInfo = new RegionInfo(context, regionName, (JSONObject) parser.parse(result));

                final ListView listView = (ListView) findViewById(R.id.moreInfoListView);
                final RegionInfoAdapter adapter = new RegionInfoAdapter(context, regionInfo);
                listView.setAdapter(adapter);

            } catch (Exception e){
                Toast.makeText(getBaseContext(), context.getText(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        }
    }

}

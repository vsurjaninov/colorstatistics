package org.androwolfs.ColorStatistics;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import org.androwolfs.ColorStatistics.polygon.JSONArrayPolygon;
import org.androwolfs.ColorStatistics.polygon.PolygonInfoWindow;
import org.androwolfs.ColorStatistics.utils.HttpLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import java.util.Random;
import java.util.Set;

public class RegionsMap extends Activity {
    private MapView map;
    private Context context = this;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regions_map);

        map = (MapView) findViewById(R.id.mapview);
        map.setTileSource(TileSourceFactory.MAPQUESTOSM);
        map.setMultiTouchControls(true);

        IMapController mapController = map.getController();
        mapController.setZoom(11);
        mapController.setCenter(new GeoPoint(59.8944444, 30.2641667));
        Toast.makeText(getBaseContext(), context.getText(R.string.loading), Toast.LENGTH_LONG).show();
        if(HttpLoader.isConnected(this)){
            new HttpAsyncTask().execute("http://androwolfs.cloudapp.net/polygons");
        } else {
            Toast.makeText(getBaseContext(), context.getText(R.string.connection_error), Toast.LENGTH_LONG).show();
        }
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return HttpLoader.GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            try{
                final JSONParser parser = new JSONParser();
                final JSONObject jsonRegions = (JSONObject)parser.parse(result);
                final Set regions = jsonRegions.keySet();

                for(Object region:regions){
                    final JSONArrayPolygon polygon = new JSONArrayPolygon(map.getContext());
                    polygon.setJSONPoints((JSONArray)jsonRegions.get(region));

                    PolygonInfoWindow infoWindow = new PolygonInfoWindow(map, (String)region);
                    polygon.setInfoWindow(infoWindow);

                    final Random rnd = new Random();
                    final int intColor = Color.argb(64, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                    polygon.setFillColor(intColor);
                    polygon.setStrokeColor(getResources().getColor(R.color.dark));
                    polygon.setStrokeWidth(2);

                    map.getOverlays().add(polygon);
                }
                map.invalidate();
                Toast.makeText(getBaseContext(), context.getText(R.string.ready), Toast.LENGTH_LONG).show();

            } catch (Exception e){
                Toast.makeText(getBaseContext(), context.getText(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        }
    }
}

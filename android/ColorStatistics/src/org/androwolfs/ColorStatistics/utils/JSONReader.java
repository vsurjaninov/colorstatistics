package org.androwolfs.ColorStatistics.utils;

import android.content.Context;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;

public class JSONReader {

    public static JSONObject getJSONObject(Context ctx, int rawResourceID) throws IOException, ParseException {
        final InputStream is = ctx.getResources().openRawResource(rawResourceID);
        final JSONParser parser = new JSONParser();
        return (JSONObject) parser.parse(InputStreamConverter.getString(is));
    }
}
package org.androwolfs.ColorStatistics.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class InputStreamConverter {
    public static String getString(InputStream is) throws IOException {

        final BufferedReader streamReader = new BufferedReader(new InputStreamReader(is));
        final StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;

        while ((inputStr = streamReader.readLine()) != null)
            responseStrBuilder.append(inputStr);

        streamReader.close();
        is.close();
        return responseStrBuilder.toString();
    }
}

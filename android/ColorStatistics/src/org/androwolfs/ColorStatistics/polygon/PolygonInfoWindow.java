package org.androwolfs.ColorStatistics.polygon;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.androwolfs.ColorStatistics.MoreInfo;
import org.androwolfs.ColorStatistics.R;
import org.osmdroid.bonuspack.overlays.InfoWindow;
import org.osmdroid.views.MapView;

public class PolygonInfoWindow extends InfoWindow {

    final private LinearLayout linearLayout = (LinearLayout) mView.findViewById(R.layout.bubble_layout);
    final private TextView bubbleTextView = (TextView) mView.findViewById(R.id.bubbleTextView);
    final private TextView itemCounter = (TextView) mView.findViewById(R.id.item_counter);
    final private String name;

    public PolygonInfoWindow(MapView mapView, String regionName){
        super(R.layout.bubble_layout, mapView);
        bubbleTextView.setText(regionName);
        this.name = regionName;
        itemCounter.setVisibility(View.INVISIBLE);

        ImageView bubbleCloseImageView = (ImageView) mView.findViewById(R.id.bubbleCloseImageView);
        bubbleCloseImageView.setClickable(true);
        bubbleCloseImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mView.setVisibility(View.INVISIBLE);
            }
        });

        ImageView bubbleMoreInfoImageView = (ImageView) mView.findViewById(R.id.bubbleMoreInfoImageView);
        bubbleMoreInfoImageView.setClickable(true);

        bubbleMoreInfoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mView.getContext(), MoreInfo.class);
                intent.putExtra("region_name", name);
                mView.getContext().startActivity(intent);
            }
        });
    }

    public PolygonInfoWindow(MapView mapView, String regionName, String value){
        super(R.layout.bubble_layout, mapView);
        bubbleTextView.setText(regionName);
        name = regionName;
        itemCounter.setText(value);

        ImageView bubbleCloseImageView = (ImageView) mView.findViewById(R.id.bubbleCloseImageView);
        bubbleCloseImageView.setClickable(true);
        bubbleCloseImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mView.setVisibility(View.INVISIBLE);
            }
        });

        ImageView bubbleMoreInfoImageView = (ImageView) mView.findViewById(R.id.bubbleMoreInfoImageView);
        bubbleMoreInfoImageView.setClickable(true);

        bubbleMoreInfoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mView.getContext(), MoreInfo.class);
                intent.putExtra("region_name", name);
                mView.getContext().startActivity(intent);
            }
        });
    }

    public void onOpen(Object o){
        mView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClose(){
    }
}

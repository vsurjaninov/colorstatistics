package org.androwolfs.ColorStatistics.polygon;

import android.content.Context;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osmdroid.bonuspack.overlays.Polygon;
import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;

public class JSONArrayPolygon extends Polygon {

    public JSONArrayPolygon(Context context){
        super(context);
    }

    public void setJSONPoints(JSONArray points){
        final ArrayList<GeoPoint> polygonPoints = new ArrayList<GeoPoint>();
        for(Object obj: points){
            JSONObject point = (JSONObject) obj;
            polygonPoints.add(new GeoPoint((Double)point.get("y"), (Double)point.get("x")));
        }
        this.setPoints(polygonPoints);
    }
}

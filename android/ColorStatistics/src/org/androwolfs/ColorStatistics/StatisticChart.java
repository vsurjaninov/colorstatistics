package org.androwolfs.ColorStatistics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.SeriesSelection;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.androwolfs.ColorStatistics.models.CategoryProperties;
import org.androwolfs.ColorStatistics.models.RegionStatistic;

import java.util.ArrayList;
import java.util.Random;

public class StatisticChart extends Activity {
    private ArrayList<RegionStatistic> regionStatistics;
    private Context context = this;

    private CategorySeries mSeries = new CategorySeries("");
    private DefaultRenderer mRenderer = new DefaultRenderer();
    private GraphicalView mChartView;
    private LinearLayout layout;
    private LinearLayout statusBar;
    private TextView itemTitle;
    private TextView itemCounter;
    private ImageView moreInfoImageView;
    private ImageView closeImageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regions_chart);

        layout = (LinearLayout) findViewById(R.id.chart);
        statusBar = (LinearLayout) findViewById(R.id.statusBar);
        statusBar.setVisibility(View.INVISIBLE);

        mChartView = ChartFactory.getPieChartView(this, mSeries, mRenderer);
        layout.addView(mChartView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));

        itemTitle = (TextView) findViewById(R.id.itemTitle);
        itemCounter = (TextView) findViewById(R.id.itemCounter);
        moreInfoImageView = (ImageView) findViewById(R.id.moreInfoImageView);
        closeImageView = (ImageView) findViewById(R.id.closeImageView);


        regionStatistics = getIntent().getParcelableArrayListExtra("regions");
        final String category = getIntent().getStringExtra("category");
        final TextView headerTextView = (TextView) findViewById(R.id.header);
        headerTextView.setText(category + " " + CategoryProperties.getUnits(context, category));


        for(RegionStatistic regionStatistic: regionStatistics){
            mSeries.add(regionStatistic.getRegionName(), Double.parseDouble(regionStatistic.getValue()));

            final Random rnd = new Random();
            final int intColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
            renderer.setColor(intColor);
            renderer.setChartValuesTextSize(18);
            mRenderer.addSeriesRenderer(renderer);
        }
        mRenderer.setAntialiasing(true);
        mRenderer.setLabelsTextSize(20);
        mRenderer.setLabelsColor(getResources().getColor(R.color.dark));
        mRenderer.setLegendTextSize(18);

        mRenderer.setClickEnabled(true);
        mChartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SeriesSelection seriesSelection = mChartView.getCurrentSeriesAndPoint();
                if (seriesSelection != null) {
                    for (int i = 0; i < mSeries.getItemCount(); i++) {
                        mRenderer.getSeriesRendererAt(i).setHighlighted(i == seriesSelection.getPointIndex());
                    }
                    mChartView.repaint();
                    statusBar.setVisibility(View.VISIBLE);
                    itemTitle.setText(regionStatistics.get(seriesSelection.getPointIndex()).getRegionName());
                    itemCounter.setText(regionStatistics.get(seriesSelection.getPointIndex()).getValue());
                }
            }
        });

        moreInfoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = mChartView.getCurrentSeriesAndPoint().getPointIndex();
                Intent intent = new Intent(context, MoreInfo.class);
                intent.putExtra("region_name", regionStatistics.get(position).getRegionName());
                context.startActivity(intent);
            }
        });

        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusBar.setVisibility(View.INVISIBLE);
                int position = mChartView.getCurrentSeriesAndPoint().getPointIndex();
                mRenderer.getSeriesRendererAt(position).setHighlighted(false);
            }
        });

        mChartView.repaint();
    }

}

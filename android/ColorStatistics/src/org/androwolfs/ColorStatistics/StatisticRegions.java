package org.androwolfs.ColorStatistics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import org.androwolfs.ColorStatistics.adapters.RegionsStatisticAdapter;
import org.androwolfs.ColorStatistics.models.CategoryProperties;
import org.androwolfs.ColorStatistics.models.RegionStatistic;
import org.androwolfs.ColorStatistics.utils.HttpLoader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class StatisticRegions extends Activity{

    private String category;
    final private ArrayList<RegionStatistic> regionStatistics
            = new ArrayList<RegionStatistic>();
    private Context context = this;
    private ListView listView;
    private Button buttonMap;
    private Button buttonChart;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regions_statistic);
        category = getIntent().getStringExtra("selected_category");
        final TextView headerTextView = (TextView) findViewById(R.id.header);
        headerTextView.setText(category + " " + CategoryProperties.getUnits(context, category));
        listView = (ListView) findViewById(R.id.statListView);
        buttonMap = (Button) findViewById(R.id.toStatisticMap);
        buttonMap.setVisibility(View.INVISIBLE);
        buttonChart = (Button) findViewById(R.id.toChart);
        buttonChart.setVisibility(View.INVISIBLE);

        if(HttpLoader.isConnected(this)){
            new HttpAsyncTask().execute("http://androwolfs.cloudapp.net/statistic");
        } else {
            Toast.makeText(getBaseContext(), context.getText(R.string.connection_error), Toast.LENGTH_LONG).show();
        }
    }

    public void toMap(View view){
        Intent intent = new Intent(view.getContext(), StatisticMap.class);
        intent.putExtra("regions", regionStatistics);
        intent.putExtra("category", category);
        startActivity(intent);
    }

    public void toChart(View view){
        Intent intent = new Intent(view.getContext(), StatisticChart.class);
        intent.putExtra("regions", regionStatistics);
        intent.putExtra("category", category);
        startActivity(intent);
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return HttpLoader.GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {

            try{
                final JSONParser parser = new JSONParser();
                final JSONObject jsonStatistic = (JSONObject) parser.parse(result);
                final Set jsonRegions = jsonStatistic.keySet();

                for(Object region: jsonRegions){
                    JSONObject regionInfo = (JSONObject) jsonStatistic.get(region);
                    String value = (String)regionInfo.get(category);
                    if(value != null){
                        regionStatistics.add(new RegionStatistic((String)region, value));
                    } else {
                        regionStatistics.add(new RegionStatistic((String)region, "0"));
                    }
                }

                Collections.sort(regionStatistics);
                Collections.reverse(regionStatistics);

                final RegionsStatisticAdapter adapter = new RegionsStatisticAdapter(context, regionStatistics);
                listView.setAdapter(adapter);
                buttonMap.setVisibility(View.VISIBLE);
                buttonChart.setVisibility(View.VISIBLE);

            } catch (Exception e){
                Toast.makeText(getBaseContext(), context.getText(R.string.server_error), Toast.LENGTH_LONG).show();
            }
        }

     }
}

package org.androwolfs.ColorStatistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import org.androwolfs.ColorStatistics.R;
import org.androwolfs.ColorStatistics.models.RegionStatistic;

import java.util.ArrayList;

public class RegionsStatisticAdapter extends ArrayAdapter<RegionStatistic> {

    private final Context context;
    private final ArrayList<RegionStatistic> models;

    public RegionsStatisticAdapter(Context context, ArrayList<RegionStatistic> regionStatistics) {
        super(context, R.layout.category_item, regionStatistics);
        this.models = regionStatistics;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.category_item, parent, false);
        TextView titleView = (TextView) rowView.findViewById(R.id.item_title);
        TextView counterView = (TextView) rowView.findViewById(R.id.item_counter);
        titleView.setText(models.get(position).getRegionName());
        counterView.setText(models.get(position).getValue());
        return rowView;
    }
}


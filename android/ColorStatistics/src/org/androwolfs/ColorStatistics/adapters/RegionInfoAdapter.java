package org.androwolfs.ColorStatistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import org.androwolfs.ColorStatistics.R;
import org.androwolfs.ColorStatistics.models.InfoModel;
import org.androwolfs.ColorStatistics.models.RegionInfo;

import java.util.ArrayList;

public class RegionInfoAdapter extends ArrayAdapter<InfoModel> {

    private final Context context;
    private final ArrayList<InfoModel> models;

    public RegionInfoAdapter(Context context, ArrayList<InfoModel> infoModels) {
        super(context, R.layout.category_item, infoModels);
        this.models = infoModels;
        this.context = context;
    }

    public RegionInfoAdapter(Context context, RegionInfo regionInfo) {
        super(context, R.layout.category_item, InfoModel.createInfoModels(context, regionInfo));
        this.models = InfoModel.createInfoModels(context, regionInfo);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.info_item, parent, false);
        TextView titleView = (TextView) rowView.findViewById(R.id.item_title);
        TextView counterView = (TextView) rowView.findViewById(R.id.item_counter);
        titleView.setText(models.get(position).getCategory());
        counterView.setText(models.get(position).getValue());
        return rowView;
    }
}
